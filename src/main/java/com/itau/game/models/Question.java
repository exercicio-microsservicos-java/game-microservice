package com.itau.game.models;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

//import com.itau.game.logs.Trackable;

@Entity
public class Question {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String title;
	private String category;
	private String option1;
	private String option2;
	private String option3;
	private String option4;
	private String answer;
	
	
	public Question() {
	}
	
	public Question(String title, String category, String option1, String option2, String option3,
			String option4, String answer) {
		this.title = title;
		this.category = category;
		this.option1 = option1;
		this.option2 = option2;
		this.option3 = option3;
		this.option4 = option4;
		this.answer = answer;
	}
		
	@Override
	public String toString() {
		return "Question [title=" + title + ", category=" + category + ", option1=" + option1 + ", option2=" + option2
				+ ", option3=" + option3 + ", option4=" + option4 + ", answer=" + answer + "]";
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getOption1() {
		return option1;
	}
	public void setOption1(String option1) {
		this.option1 = option1;
	}
	public String getOption2() {
		return option2;
	}
	public void setOption2(String option2) {
		this.option2 = option2;
	}
	public String getOption3() {
		return option3;
	}
	public void setOption3(String option3) {
		this.option3 = option3;
	}
	public String getOption4() {
		return option4;
	}
	public void setOption4(String option4) {
		this.option4 = option4;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}

//	@Override
//	public String getTrackedType() {
//		return Question.class.getSimpleName();
//	}
//
//	@Override
//	public Map<String, String> getTrackedProperties() {
//		Map<String, String>  map = new HashMap<String, String>();
//		map.put("id", String.valueOf(id));
//		map.put("title", String.valueOf(title));
//		map.put("category", String.valueOf(category));
//		map.put("option1", String.valueOf(option1));
//		map.put("option2", String.valueOf(option2));
//		map.put("option3", String.valueOf(option3));
//		map.put("option4", String.valueOf(option4));
//		map.put("answer", String.valueOf(answer));
//		return map;
//	}	
}
