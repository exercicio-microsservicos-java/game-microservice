package com.itau.game.models;

public enum GameState {
	GAME_FINISHED,
	GAME_ALIVE
}
