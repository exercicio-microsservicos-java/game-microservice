
package com.itau.game.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
//import com.itau.game.logs.Trackable;

@Entity
public class Game {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String playerName;
	private int currentQuestionIndice;	
	@OneToMany(cascade=CascadeType.ALL)
	List<Question> questions;
	private int hits;
	private int misses;
	private GameState gameState;

	public Game() {
		this("", new ArrayList<Question>(), 0, 0);
	}

	public Game(String playerName, int hits, int misses) {
		this(playerName, new ArrayList<Question>(), hits, misses);
	}

	public Game(String playerName, List<Question> questions, int hits, int misses) {
		this.playerName = playerName;
		this.questions = questions;
		this.hits = hits;
		this.misses = misses;
		this.setCurrentQuestionIndice(0);
		this.setGameState(GameState.GAME_ALIVE);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	public int getHits() {
		return hits;
	}

	public void setHits(int hits) {
		this.hits = hits;
	}

	public int getMisses() {
		return misses;
	}

	public void setMisses(int misses) {
		this.misses = misses;
	}

	@JsonIgnore
	public Question getCurrentQuestion() {
		return questions.get(getCurrentQuestionIndice());
	}

	public void addAnswer(int answer) {
		if (getGameState() == GameState.GAME_ALIVE && (answer > 0 && answer <= 4)) {
			boolean isAnswerCorrect = isCorrect(getCurrentQuestion(), answer);
			if (isAnswerCorrect) {
				hits++;
			} else {
				misses++;
			} 
			
			if (getCurrentQuestionIndice() == 9) {
				setGameState(GameState.GAME_FINISHED);				
			} else {
				setCurrentQuestionIndice(getCurrentQuestionIndice() + 1);
			}
		}
	}
	
	@Override
	public String toString() {
		return "Game [playerName=" + playerName + ", currentQuestionIndice=" + currentQuestionIndice + ", questions="
				+ questions + ", hits=" + hits + ", misses=" + misses + ", gameState=" + gameState + "]";
	}

	public GameState getGameState() {
		return gameState;
	}
	
	protected void setGameState(GameState gameState) {
		this.gameState = gameState;
	}

	private boolean isCorrect(Question question, int answer) {
		return Integer.parseInt(question.getAnswer()) == answer ? true : false;
	}

	public int getCurrentQuestionIndice() {
		return currentQuestionIndice;
	}

	public void setCurrentQuestionIndice(int currentQuestionIndice) {
		this.currentQuestionIndice = currentQuestionIndice;
	}

//	@Override
//	public String getTrackedType() {
//		return Game.class.getSimpleName();
//	}
//
//	@Override
//	public Map<String, String> getTrackedProperties() {
//		Map<String, String>  map = new HashMap<String, String>();
//		map.put("id", String.valueOf(id));
//		map.put("name", String.valueOf(playerName));
//		map.put("hits", String.valueOf(hits));
//		map.put("misses", String.valueOf(misses));
//		return map;
//
//	}
}
