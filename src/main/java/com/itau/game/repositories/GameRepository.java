package com.itau.game.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.itau.game.models.Game;

@Repository
public interface GameRepository extends CrudRepository<Game, Long>{

}
