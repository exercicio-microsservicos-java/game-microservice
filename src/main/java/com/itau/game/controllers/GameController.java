package com.itau.game.controllers;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//import com.itau.game.logs.Events;
import com.itau.game.logs.GameLogReader;
import com.itau.game.logs.GameLogger;
import com.itau.game.logs.LogEngine;
import com.itau.game.models.Game;
import com.itau.game.models.GameState;
import com.itau.game.models.Question;
import com.itau.game.repositories.GameRepository;
import com.itau.game.services.ranking.RankingService;
import com.itau.game.services.question.QuestionConverter;
import com.itau.game.services.question.QuestionService;
import com.itau.game.services.ranking.RankingMessage;


@RestController
@RequestMapping("games")
@CrossOrigin
public class GameController {

	private final int GAME_TOTAL_QUESTIONS = 10;
	
	@Autowired	
	private GameRepository gameRepository;		
	
	@Autowired
	private QuestionService questionService;
	
	@Autowired
	private RankingService rankingService; 
	
//	@Autowired
//	private LogEngine logEngine;

	@PostMapping("/create")
	public ResponseEntity<Game> create(@RequestBody HashMap<String, String> playerName) {
		List<Question> questions = QuestionConverter.convertFromQuestionMessageList(questionService.getQuestions(GAME_TOTAL_QUESTIONS));		
		Game game = new Game(playerName.get("playerName"), questions, 0, 0);
		gameRepository.save(game);
		
		//GameLogger.log(MessageFormat.format("{0} created.", game.toString()));
		//Events.emmit("GamesController", "create", game);
		
		return ResponseEntity.ok(game);
	}

	@GetMapping("/{gameId}/current-question")
	public ResponseEntity<?> getCurrentQuestion(@PathVariable Long gameId) {
		Optional<Game> gameOptional = gameRepository.findById(gameId);
		if (!gameOptional.isPresent()) {
			return ResponseEntity.badRequest().build();
		}
		Game gameDb = gameOptional.get();
		if (gameDb.getGameState() == GameState.GAME_ALIVE) {
			
			//GameLogger.log(MessageFormat.format("{0} readed.", gameDb.getCurrentQuestion()));
			//Events.emmit("GamesController", "getCurrentQuestion", gameDb.getCurrentQuestion());
			
			return ResponseEntity.ok(gameDb.getCurrentQuestion());
		}
		
		return ResponseEntity.badRequest().build();
	}

	@GetMapping("/{gameId}/add-answer/{answerIndex}")
	public ResponseEntity<Game> addAnswer(@PathVariable Long gameId, @PathVariable int answerIndex) {
		Optional<Game> gameOptional = gameRepository.findById(gameId);
		if (!gameOptional.isPresent()) {
			return ResponseEntity.badRequest().build();
		}
		Game gameDb = gameOptional.get();
		Question currentQuestion = gameDb.getCurrentQuestion();
		gameDb.addAnswer(answerIndex);		
		gameRepository.save(gameDb);
		
		//GameLogger.log(MessageFormat.format("{0} answered with answer index: {1}.", currentQuestion.toString(), answerIndex));
		//Events.emmit("GamesController", "addAnswer", gameDb.getCurrentQuestion());
		
		return ResponseEntity.ok(gameDb);
	}
	
	@GetMapping("/{gameId}/save-ranking")
	public ResponseEntity<?> saveRanking(@PathVariable Long gameId) {
		Optional<Game> gameOptional = gameRepository.findById(gameId);
		if (!gameOptional.isPresent()) {
			return ResponseEntity.badRequest().build();
		}
		Game gameDb = gameOptional.get();
				
		RankingMessage rankingMessage = new RankingMessage("Quiz", gameDb.getPlayerName(), gameDb.getHits());
				
		rankingService.saveRanking(rankingMessage);		

		return ResponseEntity.ok().build();
	}

	@GetMapping("/{gameId}/state")
	public ResponseEntity<?> getGameState(@PathVariable Long gameId) {
		Optional<Game> gameOptional = gameRepository.findById(gameId);
		if (!gameOptional.isPresent()) {
			return ResponseEntity.badRequest().build();
		}
		Game gameDb = gameOptional.get();
		
		HashMap<String, String> gameState = new HashMap<>();
		gameState.put("gameState", String.valueOf(gameDb.getGameState()));
		
		//GameLogger.log(MessageFormat.format("{0} gameState read.", gameDb.toString()));
		//Events.emmit("GamesController", "getGameState", gameDb);

		return ResponseEntity.ok(gameState);
	}
	
	@GetMapping("/ranking")
	public ResponseEntity<?> getRanking() {
		List<RankingMessage> rankingList = rankingService.viewRanking();
		return ResponseEntity.ok(rankingList);
	} 
	
//	@GetMapping("/logs")
//	public ResponseEntity<?> getGameLogs() {
//		return ResponseEntity.ok(GameLogReader.readLogs()); 
//	}

}
