package com.itau.game.services.ranking;

import static com.itau.game.configs.EnvContext.*;

import java.util.HashMap;
import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.client.RestTemplate;

public class RankingServiceMQ implements RankingService {

	
	private JmsTemplate jmsTemplate;
	
	public RankingServiceMQ(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}
	
	
	@Override
	public void saveRanking(RankingMessage ranking) {
		HashMap<String, String> body = new HashMap<>();

		body.put("playerName", ranking.getUsernameJogador());
		body.put("gameId", "Quiz");
		body.put("hits", String.valueOf(ranking.getPontuacao()));
		body.put("misses", String.valueOf(ranking.getMisses()));
		body.put("total", String.valueOf(ranking.getPontuacao()));

		jmsTemplate.convertAndSend(ENV_RANKING_QUEUE, body);
	}

	@Override
	public List<RankingMessage> viewRanking() {
		// TODO Auto-generated method stub
		RestTemplate restTemplate = new RestTemplate();
		
		String URL = "http://" + System.getenv("APP_RANKING_IP");
		String viewRankingEndpoint = URL + "/ranking/Quiz";

		ResponseEntity<List<RankingMessage>> rankingResponse = restTemplate.exchange(viewRankingEndpoint, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<RankingMessage>>() {

				});

		return rankingResponse.getBody();
	}

}
