package com.itau.game.services.ranking;

import java.util.List;

public interface RankingService {
	
	public void saveRanking(RankingMessage ranking);
	public List<RankingMessage> viewRanking();
	
}
