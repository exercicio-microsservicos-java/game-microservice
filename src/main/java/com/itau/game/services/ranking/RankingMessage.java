package com.itau.game.services.ranking;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class RankingMessage {
	private String nomeJogo;
	private String usernameJogador;
	private int pontuacao;
	@JsonIgnore
	private String misses;
	
	public RankingMessage() {
	}
	
	public RankingMessage(String nomeJogo, String usernameJogador, int pontuacao) {
		this.nomeJogo = nomeJogo;
		this.usernameJogador = usernameJogador;
		this.pontuacao = pontuacao;
	}
	public String getNomeJogo() {
		return nomeJogo;
	}
	public void setNomeJogo(String nomeJogo) {
		this.nomeJogo = nomeJogo;
	}
	public String getUsernameJogador() {
		return usernameJogador;
	}
	public void setUsernameJogador(String usernameJogador) {
		this.usernameJogador = usernameJogador;
	}
	public int getPontuacao() {
		return pontuacao;
	}
	public void setPontuacao(int pontuacao) {
		this.pontuacao = pontuacao;
	}

	public String getMisses() {
		return misses;
	}

	public void setMisses(String misses) {
		this.misses = misses;
	}
}
