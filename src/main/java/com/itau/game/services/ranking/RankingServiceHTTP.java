package com.itau.game.services.ranking;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class RankingServiceHTTP implements RankingService {
	private RestTemplate restTemplate = new RestTemplate();
	
	@Override
	public void saveRanking(RankingMessage ranking) {
		String URL = "http://" + System.getenv("APP_RANKING_IP");
		String saveRankingEndpoint = URL + "/gravarPontuacao";

		restTemplate.postForObject(saveRankingEndpoint, ranking, RankingMessage.class);

	}

	@Override
	public List<RankingMessage> viewRanking() {
		String URL = "http://" + System.getenv("APP_RANKING_IP");
		String viewRankingEndpoint = URL + "/ranking/Quiz";

		ResponseEntity<List<RankingMessage>> rankingResponse = restTemplate.exchange(viewRankingEndpoint, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<RankingMessage>>() {

				});

		return rankingResponse.getBody();
	}
}
