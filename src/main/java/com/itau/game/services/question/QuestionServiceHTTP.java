package com.itau.game.services.question;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class QuestionServiceHTTP implements QuestionService {
	private RestTemplate restTemplate = new RestTemplate();
	
	@Override
	public List<QuestionMessage> getQuestions(int size) {
		String URL = "http://" + System.getenv("APP_QUESTION_IP");

		String questionsEndpoint = URL + "/questions/" + size;

		ResponseEntity<List<QuestionMessage>> rateResponse = restTemplate.exchange(questionsEndpoint, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<QuestionMessage>>() {
				});

		return rateResponse.getBody();
	}

}
