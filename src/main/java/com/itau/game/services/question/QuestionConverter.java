package com.itau.game.services.question;

import java.util.ArrayList;
import java.util.List;

import com.itau.game.models.Question;

public class QuestionConverter {

	public static List<Question> convertFromQuestionMessageList(List<QuestionMessage> questionMessages) {
		List<Question> questions = new ArrayList<Question>();
		
		for (QuestionMessage questionMessage : questionMessages) {
			questions.add(new Question(questionMessage.getTitle(), 
					questionMessage.getCategory(), 
					questionMessage.getOption1(), 
					questionMessage.getOption2(), 
					questionMessage.getOption3(), 
					questionMessage.getOption4(), 
					questionMessage.getAnswer()));
		}
		
		return questions;
	}
	
	public static Question convertFromQuestionMessage(QuestionMessage questionMessage) {
		Question questionReturn = new Question(questionMessage.getTitle(), 
					questionMessage.getCategory(), 
					questionMessage.getOption1(), 
					questionMessage.getOption2(), 
					questionMessage.getOption3(), 
					questionMessage.getOption4(), 
					questionMessage.getAnswer());
		
		return questionReturn;
	}
	
}
