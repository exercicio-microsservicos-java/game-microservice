package com.itau.game.services.question;

import static com.itau.game.configs.EnvContext.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;

public class QuestionServiceMQ implements QuestionService {

	@Override
	public List<QuestionMessage> getQuestions(int size) {
		// Abrir conexão com o broker
		ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(ENV_AMQ_SERVER);
		Connection connection;
		List<QuestionMessage> questions = new ArrayList<QuestionMessage>();

		try {
			connection = connectionFactory.createConnection();
			connection.start();

			// Criar uma sessão
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

			// A fila que queremos popular
			Queue queue = session.createQueue(ENV_QUESTION_QUEUE);

			// Nosso produtor de eventos
			MessageProducer producer = session.createProducer(queue);

			// Criar uma fila temporária
			Destination tempQueue = session.createTemporaryQueue();

			// Criar o consumidor da fila temporária
			MessageConsumer responseConsumer = session.createConsumer(tempQueue);

			MapMessage msg = session.createMapMessage();
			msg.setString("quantity", String.valueOf(size));
			msg.setString("random", "yes");

			// Configurar os parâmetros de resposta
			msg.setJMSCorrelationID("val-req-" + String.valueOf(size));
			msg.setJMSReplyTo(tempQueue);

			// Envia a mensagem
			producer.send(msg);

			// Get the response
			MapMessage response = (MapMessage) responseConsumer.receive(10000);

			List<Map<String, String>> maplist = (List<Map<String, String>>) response.getObject("questions");

			for (Map<String, String> map : maplist) {
				String title = map.get("title");
				String category = map.get("category");
				String option1 = map.get("option1");
				String option2 = map.get("option2");
				String option3 = map.get("option3");
				String option4 = map.get("option4");
				String answer = map.get("answer");
				questions.add(new QuestionMessage(title, category, option1, option2, option3, option4, answer));
			}

			return questions;

		} catch (JMSException e) {
			e.printStackTrace();
			return null;
		}
	}

}
