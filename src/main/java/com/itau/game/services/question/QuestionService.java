package com.itau.game.services.question;

import java.util.List;

public interface QuestionService {
	public List<QuestionMessage> getQuestions(int size);	
}
