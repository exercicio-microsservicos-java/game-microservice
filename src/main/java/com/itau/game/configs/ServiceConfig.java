package com.itau.game.configs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.itau.game.services.question.QuestionService;
import com.itau.game.services.question.QuestionServiceHTTP;
import com.itau.game.services.question.QuestionServiceMQ;
import com.itau.game.services.ranking.RankingService;
import com.itau.game.services.ranking.RankingServiceHTTP;
import com.itau.game.services.ranking.RankingServiceMQ;

@Component
public class ServiceConfig {
	
	@Autowired
	private JmsTemplate jmsTemplate;
	
	@Bean
	@Value("${APP_MESSAGE_TYPE}")
	public RankingService getRankingService(MessageType messageType) {
		switch (messageType) {
			case HTTP:
				return new RankingServiceHTTP();
			case MQ:
				return new RankingServiceMQ(jmsTemplate);
			default:
				return null;
		}
	}

	@Bean
	@Value("${APP_MESSAGE_TYPE}")
	public QuestionService getQuestionService(MessageType messageType) {
		switch (messageType) {
			case HTTP:
				return new QuestionServiceHTTP();
			case MQ:
				return new QuestionServiceMQ();
			default:
				return null;
		}
	}
	
}
