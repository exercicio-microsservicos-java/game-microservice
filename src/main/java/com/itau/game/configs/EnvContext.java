package com.itau.game.configs;

public class EnvContext {
	// Environment message type: HTTP/MQ
	public static final MessageType ENV_MESSAGE_TYPE = MessageType.valueOf(System.getenv("APP_MESSAGE_TYPE"));
	
	// Environment MQ configurations
	public static final String ENV_AMQ_SERVER = System.getenv("APP_AMQ_SERVER");
	public static final String ENV_QUESTION_QUEUE =  System.getenv("APP_QUESTION_QUEUE");	
	public static final String ENV_RANKING_QUEUE = System.getenv("APP_RANKING_QUEUE");
}
