package com.itau.game.controllers;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.http.MediaType;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itau.game.models.Game;
import com.itau.game.models.GameState;
import com.itau.game.models.Question;
import com.itau.game.repositories.GameRepository;
import com.itau.game.services.question.QuestionConverter;
import com.itau.game.services.question.QuestionMessage;
import com.itau.game.services.question.QuestionService;
import com.itau.game.services.ranking.RankingMessage;
import com.itau.game.services.ranking.RankingService;

@RunWith(SpringRunner.class)
@WebMvcTest(value = GameController.class)
public class GameControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private GameRepository gameRepository;

	@MockBean
	private QuestionService questionService;

	@MockBean
	private RankingService rankingService;

	public GameControllerTest() {

	}

	@Test
	public void testCreateGame() throws Exception {
		// Mocks
		ArrayList<QuestionMessage> mockQuestionMessages = new ArrayList<QuestionMessage>();
		mockQuestionMessages.add(new QuestionMessage("Pergunta 1", "", "Opcao1", "Opcao2", "Opcao3", "Opcao4", "1"));
		mockQuestionMessages.add(new QuestionMessage("Pergunta 2", "", "Opcao1", "Opcao2", "Opcao3", "Opcao4", "2"));
		mockQuestionMessages.add(new QuestionMessage("Pergunta 3", "", "Opcao1", "Opcao2", "Opcao3", "Opcao4", "3"));
		mockQuestionMessages.add(new QuestionMessage("Pergunta 4", "", "Opcao1", "Opcao2", "Opcao3", "Opcao4", "4"));

		List<Question> mockQuestions = QuestionConverter.convertFromQuestionMessageList(mockQuestionMessages);

		Game gameMock = new Game("Daniel", mockQuestions, 0, 0);

		// Whens
		when(questionService.getQuestions(anyInt())).thenReturn(mockQuestionMessages);
		when(gameRepository.save(any(Game.class))).thenReturn(gameMock);

		// Execute
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/games/create").accept(MediaType.APPLICATION_JSON)
				.content("{ \"playerName\": \"Daniel\" }").contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		String jsonResult = result.getResponse().getContentAsString();
		ObjectMapper mapper = new ObjectMapper();

		Game gameExpected = new Game("Daniel", mockQuestions, 0, 0);

		String jsonExpected = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(gameExpected);

		// Assert
		JSONAssert.assertEquals(jsonExpected, jsonResult, false);
	}

	@Test
	public void testGetCurrentQuestion() throws Exception {
		// Mocks
		ArrayList<QuestionMessage> questionMessagesMock = new ArrayList<QuestionMessage>();
		QuestionMessage currentQuestionMessageMock = new QuestionMessage("Pergunta 1", "", "Opcao1", "Opcao2", "Opcao3",
				"Opcao4", "1");
		questionMessagesMock.add(currentQuestionMessageMock);
		questionMessagesMock.add(new QuestionMessage("Pergunta 2", "", "Opcao1", "Opcao2", "Opcao3", "Opcao4", "2"));
		questionMessagesMock.add(new QuestionMessage("Pergunta 3", "", "Opcao1", "Opcao2", "Opcao3", "Opcao4", "3"));
		questionMessagesMock.add(new QuestionMessage("Pergunta 4", "", "Opcao1", "Opcao2", "Opcao3", "Opcao4", "4"));
		Question currentQuestionMock = QuestionConverter.convertFromQuestionMessage(currentQuestionMessageMock);
		List<Question> questionsMock = QuestionConverter.convertFromQuestionMessageList(questionMessagesMock);

		Game gameMock = new Game("Daniel", questionsMock, 0, 0);
		Optional<Game> optionalGameMock = Optional.of(gameMock);

		// Whens
		when(gameRepository.findById(anyLong())).thenReturn(optionalGameMock);

		// Execute
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/games/1/current-question")
				.accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		String jsonResult = result.getResponse().getContentAsString();
		ObjectMapper mapper = new ObjectMapper();

		Question currentQuestionExpected = new Question("Pergunta 1", "", "Opcao1", "Opcao2", "Opcao3", "Opcao4", "1");
		String jsonExpected = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(currentQuestionExpected);

		// Assert
		JSONAssert.assertEquals(jsonExpected, jsonResult, false);
	}

	@Test
	public void testAddAnswer() throws Exception {
		// Mocks
		ArrayList<QuestionMessage> questionMessagesMock = new ArrayList<QuestionMessage>();
		questionMessagesMock.add(new QuestionMessage("Pergunta 1", "", "Opcao1", "Opcao2", "Opcao3", "Opcao4", "1"));
		questionMessagesMock.add(new QuestionMessage("Pergunta 2", "", "Opcao1", "Opcao2", "Opcao3", "Opcao4", "2"));
		questionMessagesMock.add(new QuestionMessage("Pergunta 3", "", "Opcao1", "Opcao2", "Opcao3", "Opcao4", "3"));
		questionMessagesMock.add(new QuestionMessage("Pergunta 4", "", "Opcao1", "Opcao2", "Opcao3", "Opcao4", "4"));
		List<Question> questionsMock = QuestionConverter.convertFromQuestionMessageList(questionMessagesMock);

		Game gameMock = new Game("Daniel", questionsMock, 0, 0);
		Optional<Game> optionalGameMock = Optional.of(gameMock);

		// Whens
		when(gameRepository.findById(anyLong())).thenReturn(optionalGameMock);

		// Execute
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/games/1/add-answer/1")
				.accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		String jsonResult = result.getResponse().getContentAsString();
		ObjectMapper mapper = new ObjectMapper();
		Game gameExpected = new Game("Daniel", questionsMock, 0, 0);
		gameExpected.setHits(1);
		gameExpected.setCurrentQuestionIndice(1);

		String jsonExpected = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(gameExpected);

		// Assert
		JSONAssert.assertEquals(jsonExpected, jsonResult, false);
	}

	@Test
	public void testSaveRanking() throws Exception {
		// Mocks
		ArrayList<QuestionMessage> questionMessagesMock = new ArrayList<QuestionMessage>();
		questionMessagesMock.add(new QuestionMessage("Pergunta 1", "", "Opcao1", "Opcao2", "Opcao3", "Opcao4", "1"));
		questionMessagesMock.add(new QuestionMessage("Pergunta 2", "", "Opcao1", "Opcao2", "Opcao3", "Opcao4", "2"));
		questionMessagesMock.add(new QuestionMessage("Pergunta 3", "", "Opcao1", "Opcao2", "Opcao3", "Opcao4", "3"));
		questionMessagesMock.add(new QuestionMessage("Pergunta 4", "", "Opcao1", "Opcao2", "Opcao3", "Opcao4", "4"));
		List<Question> questionsMock = QuestionConverter.convertFromQuestionMessageList(questionMessagesMock);

		Game gameMock = new Game("Daniel", questionsMock, 8, 0);
		Optional<Game> optionalGameMock = Optional.of(gameMock);

		// Whens
		when(gameRepository.findById(anyLong())).thenReturn(optionalGameMock);
		doNothing().when(rankingService).saveRanking(any(RankingMessage.class));

		// Execute and Assert
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/games/1/save-ranking")
				.accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON);

		mockMvc.perform(requestBuilder).andExpect(status().is(200));
	}

	@Test
	public void testGetGameState() throws Exception {
		// Mocks
		ArrayList<QuestionMessage> questionMessagesMock = new ArrayList<QuestionMessage>();
		questionMessagesMock.add(new QuestionMessage("Pergunta 1", "", "Opcao1", "Opcao2", "Opcao3", "Opcao4", "1"));
		questionMessagesMock.add(new QuestionMessage("Pergunta 2", "", "Opcao1", "Opcao2", "Opcao3", "Opcao4", "2"));
		questionMessagesMock.add(new QuestionMessage("Pergunta 3", "", "Opcao1", "Opcao2", "Opcao3", "Opcao4", "3"));
		questionMessagesMock.add(new QuestionMessage("Pergunta 4", "", "Opcao1", "Opcao2", "Opcao3", "Opcao4", "4"));
		List<Question> questionsMock = QuestionConverter.convertFromQuestionMessageList(questionMessagesMock);

		Game gameMock = new Game("Daniel", questionsMock, 0, 0);
		Optional<Game> optionalGameMock = Optional.of(gameMock);

		// Whens
		when(gameRepository.findById(anyLong())).thenReturn(optionalGameMock);

		// Execute and Assert
		HashMap<String, String> gameStateExpected = new HashMap<>();
		gameStateExpected.put("gameState", "GAME_ALIVE");

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/games/1/state").accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		String jsonResult = result.getResponse().getContentAsString();

		ObjectMapper mapper = new ObjectMapper();
		String jsonExpected = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(gameStateExpected);

		JSONAssert.assertEquals(jsonExpected, jsonResult, false);
	}

	@Test
	public void testGetRanking() throws Exception {
		// Mocks
		ArrayList<RankingMessage> rankingMessagesMock = new ArrayList<>();
		rankingMessagesMock.add(new RankingMessage("Quiz", "Daniel", 8));
		rankingMessagesMock.add(new RankingMessage("Quiz", "Rharamys", 8));
		rankingMessagesMock.add(new RankingMessage("Quiz", "Roberta", 8));
		
		// Whens
		when(rankingService.viewRanking()).thenReturn(rankingMessagesMock);

		// Execute and Assert
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/games/ranking").accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		String jsonResult = result.getResponse().getContentAsString();
		
		ArrayList<RankingMessage> rankingMessagesExpected = new ArrayList<>();
		rankingMessagesExpected.add(new RankingMessage("Quiz", "Daniel", 8));
		rankingMessagesExpected.add(new RankingMessage("Quiz", "Rharamys", 8));
		rankingMessagesExpected.add(new RankingMessage("Quiz", "Roberta", 8));

		ObjectMapper mapper = new ObjectMapper();
		String jsonExpected = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(rankingMessagesExpected);

		JSONAssert.assertEquals(jsonExpected, jsonResult, false);
	}

}
